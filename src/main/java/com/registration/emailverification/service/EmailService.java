package com.registration.emailverification.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.registration.emailverification.dao.ConfirmationTokenDao;
import com.registration.emailverification.dao.UserDao;
import com.registration.emailverification.entity.ConfirmationToken;
import com.registration.emailverification.entity.User;

@Service
public class EmailService {

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	UserDao userDao;

	@Autowired
	ConfirmationTokenDao confirmationTokenDao;

	public void sendEmail(SimpleMailMessage emailMessage) {
		javaMailSender.send(emailMessage);
	}

	// find user by email id
	public User findByEmailId(String emailId) {
		User user = userDao.findByEmail(emailId);
		return user;
	}

	// save user
	public void addUser(User user) {
		userDao.save(user);
	}

	// save confirmation token
	public void saveConfirmationToken(ConfirmationToken confirmationToken) {
		confirmationTokenDao.save(confirmationToken);
	}

}
