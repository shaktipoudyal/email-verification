package com.registration.emailverification.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.cors().and().csrf().disable().authorizeRequests().antMatchers("/api/**").permitAll();

		http.authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/api/register")
				.loginProcessingUrl("/api/register").permitAll().and().logout().permitAll();
	}

}
