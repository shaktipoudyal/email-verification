package com.registration.emailverification.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.registration.emailverification.entity.ConfirmationToken;

@Repository
public interface ConfirmationTokenDao extends CrudRepository<ConfirmationToken, Long> {

	public ConfirmationToken findByConfirmationToken(String confirmationToken);

}
