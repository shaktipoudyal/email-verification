package com.registration.emailverification.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.registration.emailverification.entity.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {

	public User findByEmail(String email);

}
