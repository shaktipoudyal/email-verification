package com.registration.emailverification.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.registration.emailverification.entity.ConfirmationToken;
import com.registration.emailverification.entity.User;
import com.registration.emailverification.service.EmailService;

@Controller
@RequestMapping("/api")
public class EmailController {

	@Autowired
	EmailService emailService;

	// display registration form
	@RequestMapping(method = RequestMethod.GET, value = "/register")
	public ModelAndView registerPage(ModelAndView modelAndView, User user) {
		System.out.println("register called ---" + user.toString());
		modelAndView.addObject("user", user);
		modelAndView.setViewName("register");
		return modelAndView;
	}

	// since @RequestBody isn't given for incoming user object, can not test using
	// postman..?
	// using thymeleaf, we bind this data from form
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public ModelAndView registerUser(ModelAndView modelAndView, User user) {

		String email = null;
		if (user != null) {
			email = user.getEmail();
		}

		User existingUser = emailService.findByEmailId(email);
		System.out.println("existingUser: " + existingUser);
		if (existingUser != null) {
			modelAndView.addObject("message", "Email address already exists!!");
			modelAndView.setViewName("register");
		} else {
			emailService.addUser(user);
		}

		ConfirmationToken confirmationToken = new ConfirmationToken(user);

		emailService.saveConfirmationToken(confirmationToken);

		System.out.println("sending msg---");
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo("poudel.nep@gmail.com");
		mailMessage.setSubject("Panda Aaina");
		mailMessage.setText("To confirm your account, please click here : " + "www.google.com");

		emailService.sendEmail(mailMessage);
		System.out.println("finished sending msg---");

		modelAndView.addObject("emailId", user.getEmail());
		modelAndView.setViewName("success");

		return modelAndView;
	}
}